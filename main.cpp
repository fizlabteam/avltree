#include <iostream>
#include "avltree.h"

using namespace std;

int main()
{
    AVLTree<int> tree;

    for (int i=0;i<7;++i)
        tree.add_item(i);

    tree.traverse();
    return 0;
}

